#!/bin/sh

find alpine -name "*.json" | while read -r file; do
    echo "checking $file"
    jsonschema -o pretty --instance $file ci/schema.json | grep -v "SUCCESS" >> out.log 2>&1
done

[ -s "out.log" ] && {
    exit 1
}

exit 0

